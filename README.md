RPG assignment
-------
(Approach to solution with a behaviour pattern determined by strategies-- not inheritage)

## 1. Project's Title
    Object-oriented backend for a Role-Playing Game(hereby refered to as "RPG") 

## 2. Project Description
    
    - The application allows for you, as a player, to create characters of either
        Mage, Warrior, Rogue or Ranger and upgrade your character. When upgrading 
        your character, there are opportunities for changning items held, such as 
        weapon and armours. These items affect the Damage per Second your hero can 
        afflect, and your charaters personal attributes (being strength, intelligence 
        and dexterity). Theres also the opportunity of leveling up your character for 
        it to meet level requirements tied to certain weapons and increase its personal 
        attributes.

    - This project is focused on applying Java in OOP when solving an assignement with 
        a given set of spec requirements.

    - Further implementations could involve creating a Player dashboard(controllers) 
        and extending heros to have health, before creating a Game-mode where heros 
        could fight eachother. 

### Class Diagram of the Application:
<p align="center">
  <img src="RPG diagram.png" width="350" title=" RPG diagram">
</p>

## 3. How to Install and Run the Project
    The project requires Java SE 8 or newer and is run locally without any further 
    dependencies.

## 4. Tests
    Test covers blackboxes and some whiteboxes, where the blackboxes indirectly 
    tests all behaviour in the project. Control by running gearbagtest and herotest.

## 5. Special Mentions
    A special S/O to my teacher at Experis Academies Accelarate program; Nick, 
    who patiently tried to guide me the way through the spec doc.

## 6. Last Update
    20.February 2020

## 7. License
    MIT License

    Copyright (c) 2022

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

package no.noroff.accelerate;

import no.noroff.accelerate.RPG.*;
import no.noroff.accelerate.RPG.enums.ArmorType;
import no.noroff.accelerate.RPG.enums.Slot;
import no.noroff.accelerate.RPG.enums.WeaponType;
import no.noroff.accelerate.RPG.equipment.Armor;
import no.noroff.accelerate.RPG.equipment.Weapon;
import no.noroff.accelerate.RPG.exceptions.InvalidItemException;

public class Main {

    public static void main(String[] args) throws InvalidItemException {
	// write your code here
        Hero gandalf= HeroFactory.createMageWithStartingStaff("Gandalf the grey");
        System.out.println(gandalf.levelUp());
        System.out.println(gandalf.levelUp());
        System.out.println(gandalf.changeGearMaster(new Armor("Grey Cloak", Slot.BODY,3, ArmorType.CLOTH,new PersonalAttributes(1,1,5))));
        System.out.println(gandalf.getDPS());
        System.out.println(gandalf.getStats());
        System.out.println(gandalf.levelUp());
        System.out.println(gandalf.getStats());

        Hero aragorn= HeroFactory.createWarriorWithStartingSword("Aragorn");
        System.out.println(aragorn.levelUp());
        System.out.println(aragorn.getStats());
        System.out.println(aragorn.getDPS());
        System.out.println(aragorn.changeGearMaster(new Weapon("Orc Bane", Slot.WEAPON, 2,7,3,WeaponType.AXE)));
        System.out.println(aragorn.getDPS());

        Hero legolas= HeroFactory.createRangerWithStartingBow("Legolas");
        System.out.println(legolas.levelUp());
        System.out.println(legolas.getDPS());
        System.out.println(legolas.getStats());

        Hero ariya = HeroFactory.createRogueWithStartingDagger("Ariya Stark");
        System.out.println(ariya.levelUp());
        System.out.println(ariya.getDPS());
        System.out.println(ariya.getStats());

    }
}

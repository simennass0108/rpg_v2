package no.noroff.accelerate.RPG.rangerbehaviour;

import no.noroff.accelerate.RPG.AttributeStrategy;
import no.noroff.accelerate.RPG.PersonalAttributes;

public class RangerAttributeStrategy implements AttributeStrategy {
    private PersonalAttributes attributes;

    @Override
    public void setStartingAttributes() {
        attributes = new PersonalAttributes(1, 7, 1);
    }

    @Override
    public void levelUpEffectOnAttributes() {
        attributes.upgradeAttributesByThisMuch(new PersonalAttributes(1, 5, 1));
    }

    @Override
    public double getMainAttribute(PersonalAttributes attributes) {
        return attributes.getDexterity();
    }

    @Override
    public String getAllAttributesForStats() {
        String stats = "[Strength:" + String.valueOf(attributes.getStrength()) + " dexterity:" + String.valueOf(attributes.getDexterity()) + " intelligence:" + String.valueOf(attributes.getIntelligence()) + "]";
        return stats;
    }

    @Override
    public PersonalAttributes getCurrentAttributes() {
        return attributes;
    }
}



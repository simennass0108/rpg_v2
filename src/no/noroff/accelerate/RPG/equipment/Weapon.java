package no.noroff.accelerate.RPG.equipment;

import no.noroff.accelerate.RPG.enums.Slot;
import no.noroff.accelerate.RPG.enums.WeaponType;
import no.noroff.accelerate.RPG.Item;

public class Weapon extends Item {
    private WeaponType weaponType;
    private double attack;
    private double damage;

    public Weapon(String name, Slot slot, int requiredlevel, int damage, int attack, WeaponType weaponType) {
        super(name, slot, requiredlevel);
        this.weaponType = weaponType;
        this.damage=damage;
        this.attack=attack;
    }

    public double getWeaponDPS() {
        double dps=attack*damage;
        return dps;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }
}

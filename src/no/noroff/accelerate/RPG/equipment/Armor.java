package no.noroff.accelerate.RPG.equipment;

import no.noroff.accelerate.RPG.enums.ArmorType;
import no.noroff.accelerate.RPG.enums.Slot;
import no.noroff.accelerate.RPG.Item;
import no.noroff.accelerate.RPG.PersonalAttributes;

public class Armor extends Item {
    private ArmorType armorType;
    private PersonalAttributes armorAttributes;

    public Armor(String name, Slot slot, int requiredLevel, ArmorType armorType, PersonalAttributes armorAttributes) {
        super(name, slot, requiredLevel);
        this.armorType=armorType;
        this.armorAttributes=armorAttributes;
    }

    public ArmorType getType() {
        return armorType;
    }

    public PersonalAttributes getArmorAttributes() {
        return armorAttributes;
    }
}

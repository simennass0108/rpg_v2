package no.noroff.accelerate.RPG;

import no.noroff.accelerate.RPG.enums.Slot;
import no.noroff.accelerate.RPG.enums.Type;
import no.noroff.accelerate.RPG.enums.WeaponType;
import no.noroff.accelerate.RPG.equipment.Weapon;
import no.noroff.accelerate.RPG.exceptions.InvalidItemException;
import no.noroff.accelerate.RPG.magebehaviour.MageAttributeStrategy;
import no.noroff.accelerate.RPG.magebehaviour.MageItemStrategy;
import no.noroff.accelerate.RPG.rangerbehaviour.RangerAttributeStrategy;
import no.noroff.accelerate.RPG.rangerbehaviour.RangerItemStrategy;
import no.noroff.accelerate.RPG.roguebehaviour.RogueAttributeStrategy;
import no.noroff.accelerate.RPG.roguebehaviour.RogueItemStrategy;
import no.noroff.accelerate.RPG.warriorbehaviour.WarriorAttributeStrategy;
import no.noroff.accelerate.RPG.warriorbehaviour.WarriorItemStrategy;

public class HeroFactory {
    public static Hero createMage(String name) {
        GearBag bag= new GearBag(new MageItemStrategy());
        Hero mage= new Hero(name, Type.MAGE, new MageAttributeStrategy(), bag);
        return mage;
    }

    public static Hero createMageWithStartingStaff(String name) throws InvalidItemException {
        GearBag bag= new GearBag(new MageItemStrategy());
        Hero mage = new Hero(name, Type.MAGE, new MageAttributeStrategy(), bag);
        mage.changeGearMaster(new Weapon("Wooden Staff", Slot.WEAPON, 0, 1, 3, WeaponType.STAFF));
        return mage;
    }

    public static Hero createWarriorWithStartingSword(String name) throws InvalidItemException {
        GearBag bag= new GearBag(new WarriorItemStrategy());
        Hero warrior = new Hero(name, Type.WARRIOR, new WarriorAttributeStrategy(), bag);
        warrior.changeGearMaster(new Weapon("Not Exactly the Excalibour", Slot.WEAPON, 0, 2, 2, WeaponType.BOW));
        return warrior;
    }
    public static Hero createRogueWithStartingDagger(String name) throws InvalidItemException {
        GearBag bag = new GearBag(new RogueItemStrategy());
        Hero rogue = new Hero(name, Type.ROUGE, new RogueAttributeStrategy(), bag);
        rogue.changeGearMaster(new Weapon("The Needle", Slot.WEAPON, 0, 5, 1, WeaponType.DAGGER));
        return rogue;
    }
    public static Hero createRangerWithStartingBow(String name) throws InvalidItemException {
        GearBag bag= new GearBag(new RangerItemStrategy());
        Hero ranger = new Hero(name, Type.RANGER, new RangerAttributeStrategy(), bag);
        ranger.changeGearMaster(new Weapon("Longbow", Slot.WEAPON, 0, 2, 3, WeaponType.BOW));
        return ranger;
    }
}

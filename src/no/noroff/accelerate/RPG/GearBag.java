package no.noroff.accelerate.RPG;

import no.noroff.accelerate.RPG.enums.Slot;
import no.noroff.accelerate.RPG.equipment.Armor;
import no.noroff.accelerate.RPG.equipment.Weapon;
import no.noroff.accelerate.RPG.exceptions.InvalidArmourException;
import no.noroff.accelerate.RPG.exceptions.InvalidItemException;
import no.noroff.accelerate.RPG.exceptions.InvalidWeaponException;
import no.noroff.accelerate.RPG.exceptions.RequiredLevelNotFullfilledException;

import java.util.HashMap;

import static no.noroff.accelerate.RPG.enums.Slot.*;

public class GearBag {
    private HashMap<Slot, Item> gearPockets;
    private ItemStrategy itemStrategy;

    public GearBag(ItemStrategy itemStrategy) {
        this.itemStrategy=itemStrategy;
        HashMap<Slot, Item> gearPockets = new HashMap<>();
        gearPockets.put(Slot.WEAPON, null);
        gearPockets.put(HEAD, null);
        gearPockets.put(BODY, null);
        gearPockets.put(LEGS, null);
        this.gearPockets=gearPockets;
    }

    public String changeGear(Item item, int currentLevel) throws InvalidItemException {
        if (!itemStrategy.checkIfRequiredLevelIsComplient(item.getRequiredLevel(),currentLevel)){   ///PLACEHOLDER .
            throw new RequiredLevelNotFullfilledException("The required level to equip this item is higher then your current level");
        }
        if (item instanceof no.noroff.accelerate.RPG.equipment.Weapon) {
            if (!itemStrategy.checkIfItemIsComplient(((no.noroff.accelerate.RPG.equipment.Weapon) item).getWeaponType())) {
                throw new InvalidWeaponException("Nah, your hero can't carry this type of weapon");
            } else {
                gearPockets.put(item.getSlot(), item);
                return "New Weapon added to your gearbag";
            }
        }
        else {
            if (!itemStrategy.checkIfItemIsComplient(((Armor) item).getType())) {
                throw new InvalidArmourException("Nah, your hero can't carry this type of weapon");
            } else {
                itemStrategy.checkIfItemIsComplient(((Armor) item).getType());
                gearPockets.put(item.getSlot(), item);
                return "New Armor added to your gearbag";
            }
        }
    }
    public double getWeaponDPS(){
        Weapon equipedWeapon= (Weapon) gearPockets.get(Slot.WEAPON);
        return equipedWeapon==null ? 1 : equipedWeapon.getWeaponDPS();
    }

    public PersonalAttributes calculateArmourAttributes() {
        Armor head = (Armor) gearPockets.get(HEAD);
        Armor body = (Armor) gearPockets.get(BODY);
        Armor legs = (Armor) gearPockets.get(LEGS);

        PersonalAttributes armorAttributes = new PersonalAttributes(0, 0, 0);

        if (head != null) {
            armorAttributes.add(head.getArmorAttributes());
        }
        if (body != null){
            armorAttributes.add(body.getArmorAttributes());
        }
        if (legs != null){
            armorAttributes.add(legs.getArmorAttributes());
        }
         return armorAttributes;
    }
}


 /*
   private Queue<Slot> createQueue(){
        Queue<Slot> slotQueue= new PriorityQueue<>();
        slotQueue.add(Head);
        slotQueue.add(Body);
        slotQueue.add(Legs);
        return slotQueue;
        }
    private void changeQueueSlot(){
        slotQueue.add(slotQueue.peek());
        slotQueue.remove();
        }
 */
package no.noroff.accelerate.RPG;

public interface AttributeStrategy {
    void setStartingAttributes();
    void levelUpEffectOnAttributes();
    double getMainAttribute(PersonalAttributes attributes);
    String getAllAttributesForStats();
    PersonalAttributes getCurrentAttributes();
}

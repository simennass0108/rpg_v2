package no.noroff.accelerate.RPG;

import no.noroff.accelerate.RPG.enums.Slot;

public class Item {
    private String name;
    private int requiredLevel;
    private Slot slot;

    public Item(String name, Slot slot, int requiredLevel) {
        this.name=name;
        this.slot=slot;
        this.requiredLevel=requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }
}

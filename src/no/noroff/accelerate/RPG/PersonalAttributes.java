package no.noroff.accelerate.RPG;

public class PersonalAttributes {
    private double strength;
    private double dexterity;
    private double intelligence;

    public PersonalAttributes(double s, double d, double i){
        this.strength=s;
        this.dexterity=d;
        this.intelligence=i;
    }
    public void upgradeAttributesByThisMuch(PersonalAttributes attributes) {
        this.strength += attributes.strength;
        this.dexterity += attributes.dexterity;
        this.intelligence += attributes.intelligence;
    }

    public double getIntelligence() {
        return intelligence;
    }

    public double getStrength() {
        return strength;
    }

    public double getDexterity() {
        return dexterity;
    }

    public PersonalAttributes add(PersonalAttributes personalAttributes){
        this.strength+=personalAttributes.getStrength();
        this.dexterity+=personalAttributes.getDexterity();
        this.intelligence+=personalAttributes.getIntelligence();
        return this;
    }
}

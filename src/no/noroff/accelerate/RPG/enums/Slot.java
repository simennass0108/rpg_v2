package no.noroff.accelerate.RPG.enums;

public enum Slot {
    HEAD,
    WEAPON,
    LEGS,
    BODY
}

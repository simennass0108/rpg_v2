package no.noroff.accelerate.RPG.enums;

public enum WeaponType {
    AXE,
    HAMMER,
    STAFF,
    BOW,
    WAND,
    DAGGER,
    SWORD
}

package no.noroff.accelerate.RPG.enums;

public enum Type {
    MAGE,
    WARRIOR,
    ROUGE,
    RANGER
}

package no.noroff.accelerate.RPG;

import no.noroff.accelerate.RPG.enums.ArmorType;
import no.noroff.accelerate.RPG.enums.WeaponType;
import no.noroff.accelerate.RPG.exceptions.InvalidArmourException;
import no.noroff.accelerate.RPG.exceptions.InvalidWeaponException;
import no.noroff.accelerate.RPG.exceptions.RequiredLevelNotFullfilledException;

public interface ItemStrategy {
    boolean checkIfItemIsComplient(WeaponType weaponType) throws InvalidWeaponException;
    boolean checkIfItemIsComplient(ArmorType armorType) throws InvalidArmourException;
    boolean checkIfRequiredLevelIsComplient(int required, int currentlevel) throws RequiredLevelNotFullfilledException;
}

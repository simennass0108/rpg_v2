package no.noroff.accelerate.RPG.warriorbehaviour;

import no.noroff.accelerate.RPG.enums.ArmorType;
import no.noroff.accelerate.RPG.enums.WeaponType;
import no.noroff.accelerate.RPG.exceptions.InvalidArmourException;
import no.noroff.accelerate.RPG.exceptions.InvalidWeaponException;
import no.noroff.accelerate.RPG.exceptions.RequiredLevelNotFullfilledException;
import no.noroff.accelerate.RPG.GearBag;
import no.noroff.accelerate.RPG.Item;
import no.noroff.accelerate.RPG.ItemStrategy;

public class WarriorItemStrategy implements ItemStrategy {
        private GearBag gearbag;
        private Item item;

        @Override
        public boolean checkIfItemIsComplient(ArmorType armorType) throws InvalidArmourException {
            if(armorType==ArmorType.MAIL || armorType==ArmorType.PLATE)
            {return true;
            }
            else{
                return false;
            }
        }

        @Override
        public boolean checkIfItemIsComplient(WeaponType weaponType) throws InvalidWeaponException {
            if(weaponType==WeaponType.AXE || weaponType==WeaponType.HAMMER || weaponType==WeaponType.SWORD)
            {return true;
            }
            else{
                return false;
            }
        }

        @Override
        public boolean checkIfRequiredLevelIsComplient(int required, int currentlevel) throws RequiredLevelNotFullfilledException {
            if(required<=currentlevel)
            {return true;
            }
            else{
                return false;
            }
        }
    }


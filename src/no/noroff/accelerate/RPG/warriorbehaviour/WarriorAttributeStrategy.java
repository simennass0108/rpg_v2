package no.noroff.accelerate.RPG.warriorbehaviour;

import no.noroff.accelerate.RPG.AttributeStrategy;
import no.noroff.accelerate.RPG.PersonalAttributes;

public class WarriorAttributeStrategy implements AttributeStrategy {
    private PersonalAttributes attributes;

    @Override
    public void setStartingAttributes() {
        attributes = new PersonalAttributes(5, 2, 1);
    }

    @Override
    public void levelUpEffectOnAttributes() {
        attributes.upgradeAttributesByThisMuch(new PersonalAttributes(3, 2, 1));
    }

    @Override
    public double getMainAttribute(PersonalAttributes attributes) {
        return attributes.getStrength();
    }

    @Override
    public String getAllAttributesForStats() {
        String stats = "[Strength:" + String.valueOf(attributes.getStrength()) + " dexterity:" + String.valueOf(attributes.getDexterity()) + " intelligence:" + String.valueOf(attributes.getIntelligence()) + "]";
        return stats;
    }

    @Override
    public PersonalAttributes getCurrentAttributes() {
        return attributes;
    }
}



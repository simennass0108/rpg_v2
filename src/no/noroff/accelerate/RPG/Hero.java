package no.noroff.accelerate.RPG;

import no.noroff.accelerate.RPG.enums.Type;
import no.noroff.accelerate.RPG.exceptions.InvalidItemException;

import java.util.ArrayList;

public class Hero{
    private String name;
    private int level=1;
    private Type characterType;
    private AttributeStrategy attributeStrategy;
    private GearBag gearBag;

    public AttributeStrategy getAttributeStrategy() {
        return attributeStrategy;
    }

    public Hero(String name, Type characterType, AttributeStrategy attributeStrategy, GearBag gearbag) {
        this.name=name;
        this.level=level;
        this.characterType=characterType;
        this.attributeStrategy=attributeStrategy;
        attributeStrategy.setStartingAttributes();
        this.gearBag=gearbag;
    }

    public int getLevel() {
        return level;
    }

    public String getStats(){
        ArrayList<String> list = new ArrayList<String>();
        list.add(name);
        list.add(String.valueOf(level));
        list.add(String.valueOf(characterType));
        list.add(attributeStrategy.getAllAttributesForStats());
        list.add(String.valueOf(getDPS()));
        return list.toString();
    }

    public String levelUp(){
        level++;
        attributeStrategy.levelUpEffectOnAttributes();
        return "Leveled up";
    }

    public String changeGearMaster(Item item) throws InvalidItemException {
        return gearBag.changeGear(item, level);
   }

   public double getDPS(){
        PersonalAttributes totalAttributes = getTotalAttributes(); // Extract main
        double totalMainAttribute=attributeStrategy.getMainAttribute(totalAttributes);
        double weaponDPS= gearBag.getWeaponDPS();
        double DPS= weaponDPS*(1+totalMainAttribute/100);
        return DPS;
   }

   private PersonalAttributes getTotalAttributes(){
        PersonalAttributes gearAttributes=gearBag.calculateArmourAttributes();
        PersonalAttributes currentAttribute=attributeStrategy.getCurrentAttributes();
        // Add the two, return it
       return currentAttribute.add(gearAttributes);
   }

}

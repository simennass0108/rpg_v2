package no.noroff.accelerate.RPG.magebehaviour;

import no.noroff.accelerate.RPG.AttributeStrategy;
import no.noroff.accelerate.RPG.PersonalAttributes;

public class MageAttributeStrategy implements AttributeStrategy {
    private PersonalAttributes attributes;

    @Override
    public void setStartingAttributes() {
        attributes = new PersonalAttributes(1, 1, 8);
    }

    @Override
    public void levelUpEffectOnAttributes() {
        attributes.upgradeAttributesByThisMuch(new PersonalAttributes(1, 1, 5));
    }

    @Override
    public double getMainAttribute(PersonalAttributes attributes) {
        return attributes.getIntelligence();
    }

    @Override
    public String getAllAttributesForStats() {
        String stats = "[Strength:" + String.valueOf(attributes.getStrength()) + " dexterity:" + String.valueOf(attributes.getDexterity()) + " intelligence:" + String.valueOf(attributes.getIntelligence()) + "]";
        return stats;
    }

    @Override
    public PersonalAttributes getCurrentAttributes() {
        return attributes;
    }
}



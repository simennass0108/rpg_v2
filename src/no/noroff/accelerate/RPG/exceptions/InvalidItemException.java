package no.noroff.accelerate.RPG.exceptions;

public class InvalidItemException extends Exception{
    public InvalidItemException(String message) {
        super(message);
    }
}

package no.noroff.accelerate.RPG.exceptions;

public class InvalidWeaponException extends InvalidItemException{
    public InvalidWeaponException(String message) {
        super(message);
    }
}

package no.noroff.accelerate.RPG.exceptions;

public class InvalidArmourException extends InvalidItemException{
    public InvalidArmourException(String message) {
        super(message);
    }
}

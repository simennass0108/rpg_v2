package no.noroff.accelerate.RPG.exceptions;

public class RequiredLevelNotFullfilledException extends InvalidItemException {
    public RequiredLevelNotFullfilledException(String message) {
        super(message);
    }
}

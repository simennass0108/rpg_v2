package no.noroff.accelerate.RPG;

import no.noroff.accelerate.RPG.enums.Slot;
import no.noroff.accelerate.RPG.enums.Type;
import no.noroff.accelerate.RPG.enums.WeaponType;
import no.noroff.accelerate.RPG.equipment.Weapon;
import no.noroff.accelerate.RPG.exceptions.InvalidItemException;
import no.noroff.accelerate.RPG.exceptions.InvalidWeaponException;
import no.noroff.accelerate.RPG.exceptions.RequiredLevelNotFullfilledException;
import no.noroff.accelerate.RPG.warriorbehaviour.WarriorAttributeStrategy;
import no.noroff.accelerate.RPG.warriorbehaviour.WarriorItemStrategy;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


class GearBagTest {

    @Test
    void changeWeapon_WarriorChangesToAxe_returnWeaponChangedString() throws InvalidItemException { //Sloppy working with a validation string...
        //Arrange
        String expMessage="New Weapon added to your gearbag";
        GearBag g = new GearBag(new WarriorItemStrategy());
        //Add
        String resMessage= g.changeGear(new Weapon("An Ax", Slot.WEAPON, 1,1,1, WeaponType.AXE),1);
        //Assert
        assertEquals(expMessage, resMessage);
    }

    @Test
    void changeWeapon_WarriorChangesToBow_ThrowInvalidWeaponException() throws InvalidItemException {
        //Arrange
        String expMessage="Nah, your hero can't carry this type of weapon";
        //Add
        Exception exception= assertThrows(InvalidWeaponException.class, ()-> {
            GearBag g = new GearBag(new WarriorItemStrategy());
            g.changeGear(new Weapon("A Bow", Slot.WEAPON, 1,1,1, WeaponType.BOW),1);
        });
        String actualMessage=exception.getMessage();
        //Assert
        assertTrue(actualMessage.contains(expMessage));
    }

    @org.junit.jupiter.api.Test
    void changeWeapon_WarriorChangesToSwordWithTooHighRequiredLevel_ThrowRequiredLevelException() throws RequiredLevelNotFullfilledException {
            //Arrange
            String expMessage="The required level to equip this item is higher then your current level";
            //Add
            Exception exception= assertThrows(RequiredLevelNotFullfilledException.class, ()-> {
                GearBag g = new GearBag(new WarriorItemStrategy());
                g.changeGear(new Weapon("A Bow", Slot.WEAPON, 99,1,1, WeaponType.SWORD),1);
            });
            String actualMessage=exception.getMessage();
            //Assert
            assertTrue(actualMessage.contains(expMessage));
    }


    @org.junit.jupiter.api.Test
    void getDPS_calculateDPSofWarriorOfLevel1WhenEquipedAxeWith2DamageAnd2Attack_return4comma2() throws InvalidItemException {
        //Arrange
        double exp_output;
        double actual_output=4.2;                                               // actual_output= (2*2)*(1+5/100)
        GearBag g = new GearBag(new WarriorItemStrategy());
        Hero w = new Hero("w", Type.WARRIOR, new WarriorAttributeStrategy(), g);
        //Add
        w.changeGearMaster(new Weapon("axe",Slot.WEAPON,1,2,2,WeaponType.AXE));
        exp_output=w.getDPS();
        //Assert
        assertEquals(exp_output,actual_output);
    }
}
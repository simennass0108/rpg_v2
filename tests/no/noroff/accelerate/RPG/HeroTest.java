package no.noroff.accelerate.RPG;

import no.noroff.accelerate.RPG.enums.Type;
import no.noroff.accelerate.RPG.magebehaviour.MageAttributeStrategy;
import no.noroff.accelerate.RPG.magebehaviour.MageItemStrategy;
import no.noroff.accelerate.RPG.rangerbehaviour.RangerAttributeStrategy;
import no.noroff.accelerate.RPG.rangerbehaviour.RangerItemStrategy;
import no.noroff.accelerate.RPG.roguebehaviour.RogueAttributeStrategy;
import no.noroff.accelerate.RPG.roguebehaviour.RogueItemStrategy;
import no.noroff.accelerate.RPG.warriorbehaviour.WarriorAttributeStrategy;
import no.noroff.accelerate.RPG.warriorbehaviour.WarriorItemStrategy;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;


class HeroTest {

    @Test
    public void  validate_setLevel1OnAllHeroesWhenCreated_shouldAllwaysReturnLevel1ForNewlyCreatedHero() {
        // Arrange
        int expectedLevel = 1;
        // Act
        Hero w = new Hero("w", Type.WARRIOR, new WarriorAttributeStrategy(), new GearBag(new WarriorItemStrategy()));
        Hero ra = new Hero("ra", Type.RANGER, new RangerAttributeStrategy(), new GearBag(new RangerItemStrategy()));
        Hero ro = new Hero("ro", Type.ROUGE, new RogueAttributeStrategy(), new GearBag(new RogueItemStrategy()));
        Hero m = new Hero("m", Type.MAGE, new MageAttributeStrategy(), new GearBag(new MageItemStrategy()));
        // Assert
        assertEquals(expectedLevel,w.getLevel());
        assertEquals(expectedLevel,ra.getLevel());
        assertEquals(expectedLevel,ro.getLevel());
        assertEquals(expectedLevel,m.getLevel());
    }

    @Test
    public void levelUp_UpdateLevelByOneAfterCreatingANewHero_shouldReturnLevel2(){
        //arrange
        int expectedLevel = 2;
        //act
        Hero w = new Hero("w", Type.WARRIOR, new WarriorAttributeStrategy(), new GearBag(new WarriorItemStrategy()));
        Hero ra = new Hero("ra", Type.RANGER, new RangerAttributeStrategy(), new GearBag(new RangerItemStrategy()));
        Hero ro = new Hero("ro", Type.ROUGE, new RogueAttributeStrategy(), new GearBag(new RogueItemStrategy()));
        Hero m = new Hero("m", Type.MAGE, new MageAttributeStrategy(), new GearBag(new MageItemStrategy()));
        w.levelUp();
        ra.levelUp();
        ro.levelUp();
        m.levelUp();
        //assert
        assertEquals(expectedLevel,w.getLevel());
        assertEquals(expectedLevel,ro.getLevel());
        assertEquals(expectedLevel,ra.getLevel());
        assertEquals(expectedLevel,m.getLevel());
    }

    @Test
    public void validate_setCorrectAttributesAfterTwoLevelUpOnAMage_returnsS3D3I18(){
        //arrange
        double expectedStrength=3;
        double expectedDexterity=3;
        double expectedIntelligence=18;
        double returnedStrength;
        double returnedDexterity;
        double returnedIntelligence;
        //act
        Hero m = new Hero("m", Type.MAGE, new MageAttributeStrategy(), new GearBag(new MageItemStrategy()));
        m.levelUp();
        m.levelUp();
        returnedStrength= m.getAttributeStrategy().getCurrentAttributes().getStrength();
        returnedDexterity=m.getAttributeStrategy().getCurrentAttributes().getDexterity();
        returnedIntelligence=m.getAttributeStrategy().getCurrentAttributes().getIntelligence();
        //assert
        assertEquals(expectedStrength,returnedStrength, 0);
        assertEquals(expectedDexterity,returnedDexterity,0);
        assertEquals(expectedIntelligence,returnedIntelligence,0);
    }

    @Test
    public void validate_setCorrectAttributesAfterTwoLevelUpOnAWarrior_returnsS11D6I3(){
        //arrange
        double expectedStrength=11;
        double expectedDexterity=6;
        double expectedIntelligence=3;
        double returnedStrength;
        double returnedDexterity;
        double returnedIntelligence;
        //act
        Hero m = new Hero("m", Type.WARRIOR, new WarriorAttributeStrategy(), new GearBag(new WarriorItemStrategy()));
        m.levelUp();
        m.levelUp();
        returnedStrength= m.getAttributeStrategy().getCurrentAttributes().getStrength();
        returnedDexterity=m.getAttributeStrategy().getCurrentAttributes().getDexterity();
        returnedIntelligence=m.getAttributeStrategy().getCurrentAttributes().getIntelligence();
        //assert
        assertEquals(expectedStrength,returnedStrength, 0);
        assertEquals(expectedDexterity,returnedDexterity,0);
        assertEquals(expectedIntelligence,returnedIntelligence,0);
    }

    @Test
    public void validate_setCorrectAttributesAfterTwoLevelUpOnARogue_returnsS4D14I3(){
        //arrange
        double expectedStrength=4;
        double expectedDexterity=14;
        double expectedIntelligence=3;
        double returnedStrength;
        double returnedDexterity;
        double returnedIntelligence;
        //act
        Hero m = new Hero("m", Type.ROUGE, new RogueAttributeStrategy(), new GearBag(new RogueItemStrategy()));
        m.levelUp();
        m.levelUp();
        returnedStrength= m.getAttributeStrategy().getCurrentAttributes().getStrength();
        returnedDexterity=m.getAttributeStrategy().getCurrentAttributes().getDexterity();
        returnedIntelligence=m.getAttributeStrategy().getCurrentAttributes().getIntelligence();
        //assert
        assertEquals(expectedStrength,returnedStrength, 0);
        assertEquals(expectedDexterity,returnedDexterity,0);
        assertEquals(expectedIntelligence,returnedIntelligence,0);
    }

    @Test
    public void validate_setCorrectAttributesAfterTwoLevelUpOnARanger_returnsS3D17I3(){
        //arrange
        double expectedStrength=3;
        double expectedDexterity=17;
        double expectedIntelligence=3;
        double returnedStrength;
        double returnedDexterity;
        double returnedIntelligence;
        //act
        Hero m = new Hero("m", Type.RANGER, new RangerAttributeStrategy(), new GearBag(new RangerItemStrategy()));
        m.levelUp();
        m.levelUp();
        returnedStrength= m.getAttributeStrategy().getCurrentAttributes().getStrength();
        returnedDexterity=m.getAttributeStrategy().getCurrentAttributes().getDexterity();
        returnedIntelligence=m.getAttributeStrategy().getCurrentAttributes().getIntelligence();
        //assert
        assertEquals(expectedStrength,returnedStrength, 0);
        assertEquals(expectedDexterity,returnedDexterity,0);
        assertEquals(expectedIntelligence,returnedIntelligence,0);
    }

}